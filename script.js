function calcular() {
    var num1 = parseInt(document.getElementById("numero1").value);
    var num2 = parseInt(document.getElementById("numero2").value);
    var resultadoDiv = document.getElementById("resultado");

    for (var i = 1; i <= 5; i++) {
        var resultado;
        switch (i) {
            case 1:
                resultado = num1 + num2;
                resultadoDiv.innerHTML += "Suma: " + resultado + "<br><br>";
                break;
            case 2:
                resultado = num1 - num2;
                resultadoDiv.innerHTML += "Resta: " + resultado + "<br><br>";
                break;
            case 3:
                resultado = num1 * num2;
                resultadoDiv.innerHTML += "Multiplicación: " + resultado + "<br><br>";
                break;
            case 4:
                resultado = num1 / num2;
                resultadoDiv.innerHTML += "División: " + resultado + "<br><br>";
                break;
            case 5:
                resultado = num1 % num2;
                resultadoDiv.innerHTML += "Módulo: " + resultado + "<br><br>";
                break;
        }
    }
}
